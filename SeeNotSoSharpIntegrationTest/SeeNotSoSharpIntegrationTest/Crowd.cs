﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeNotSoSharpIntegrationTest
{
	public class Crowd
	{
		public List<Dude> Dudes { get; set; }
		public List<Chick> Chicks { get; set; }

		public string Hello()
		{
			Console.WriteLine("Hello!");
			return "hello";
		}

	}
}
